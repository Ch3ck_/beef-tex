%----------------------------------------------------------------------------------------------
\chapter{LITERATURE REVIEW}
\label{Literature Review}
%\lhead{Chapter 2. \emph{Literature Review}}

%----------------------------------------------------------------------------------------
%      INTRODUCTION
%----------------------------------------------------------------------------------------

Health IT applications have been created and used to effectively record and manage patient medical records. Traditionally, patient data were recorded on paper records. However, advances in the field of medicine have introduced the need to manage very large amounts of data. Newly developed software tools promote meaningful use of patient health records. EMRs have improved the quality of healthcare in multiple ways namely:

\begin{itemize}
\item \textit{\texttt{Accuracy:}}  The adoption of health IT has significantly reduced health errors.
\item \textit{\texttt{Efficiency:}} EMR systems have improved the efficiency of data exchange between multiple health IT applications. They also prevent duplication of services.
\item \textit{\texttt{Better patient care:}} Data collected by an EMR application has been used to support decision-making by health care proffessionals.
\item \textit{\texttt{Understanding data about public health:}} The data captured via EMR systems have been used to create and monitor public health standards; based on demographic studies of populations over time.
\item \textit{\texttt{Record of patient care}}: Recorded patient data by the EMR systems can serve as a historical record of patient care, and is usable both as a legal record as well as means of evaluating quality of health care.
\end{itemize}


%----------------------------------------------------------------------------------------
%	Challenges to EMRS
%----------------------------------------------------------------------------------------

\section{Challenges of EMRs}

The need for standardized clinical terminology; both in variation of terms used by health care professionals, and a general lack of standardization, have both had a significant impact on the meaningful use of health IT applications. For example, a clinician in
one wing of the hospital might use the term "heart attack", while another in a different department might use the term "myocardial
infarction" to refer to the same thing. This lack of standardization reduces the quality and usefulness of the data. The most common way to handle this problem is use of standardized medical terminology.

Data privacy, confidentiality and security issues give the significance of medical data; making it extremely important that confidentiality of patient records are ensured at all times, and that access to these records is strictly controlled and is only given to relevant users. For example, different types of EMR users may only require access to certain types of data or metadata, based on their roles in the health care facility.

Challenges related to data entry makes it necessary to ensure that entering data into an EMR is efficient and easy, so that providers are able to manage their time in a productive manner. If a health care professional is overworked or distracted,
mistakes may occur that could have adverse effects on a patient's health. Integration of multiple health IT applications consistent with other efforts to ensure meaningful use of Health IT systems, data stored in the EMR system should be easily exchangeable
to and from other medical applications. For example, the integration of separate health applications into a regional or national health Information Exchange (HIE) requires that an EMR is capable of easily exchanging data with these external systems.


%--------------------------------------------------------------------------------------------------
%	Introduction to OpenMRS
%-----------------------------------------------------------------------------------------------------
\section{The OpenMRS Solution}

The mission of the OpenMRS community is to improve health care delivery in resource-constrained environments by working together as
a global community to create a robust, scalable, user-driven, open source medical record system platform. OpenMRS envisions a world where:

\begin{itemize}
\item \textit{\texttt{Models exist to implement health IT in a way that decreases costs, increases capacity, and lessens the disparities between wealthy and resource-poor environments.}}  
\item \textit{\texttt{Open standards enable people to use health IT systems to share information and reduce effort.}}
\item \textit{\texttt{Concepts and processes can easily be shared to enable health care professionals and patients work together more effectively.}}
\end{itemize}

OpenMRS software helps ease the work of health care providers and administrators to provide them with the tools to improve health outcomes all over the world.

\subsection{OpenMRS Inception and Growth}

From its humble beginnings as a solution to a problem in a small town, OpenMRS has become the largest health IT project on the planet. Between 2006 and 2012, the installation of OpenMRS at AMPATH in Kenya has recorded over 5 million heath care encounters
points of data for nearly 200,000 patients, helping to save untold thousands of lives. Every day, similar stories are retold somewhere else around the world with the assistance of thousands of volunteers. 


%--------------------------------------------------------------------------------------------

\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.3cm 0.3cm 0.2cm, clip=true, totalheight=0.3\textheight]{Figures/openmrs_community.png}
\caption[The OpenMRS community]{A vibrant OpenMRS community}
\label{OpenMRS Community}
\end{figure}

%--------------------------------------------------------------------------------------------

Today, the OpenMRS community(\cite{13}) is a widespread network of dedicated individuals and groups focused on improving the state of medical care in developing countries. As related to Health IT projects go, OpenMRS is among the biggest with over \emph{4,000,000 lines of code}, \emph {70,000+ downloads since 2010}, current present in over \textbf{169+ countries} and \textbf{6 continents}.


%----------------------------------------------------------------------------------------
%	OPENMRS ARCHITECTURE
%----------------------------------------------------------------------------------------

\subsection{OpenMRS Architecture}

OpenMRS is a framework built upon Java and other related frameworks. It is based on a modular architecture which consists of a core application and optional modules which provide additional functionality to the core workflows. The key architectural components of the OpenMRS core can be depicted as follows:

%--------------------------------------------------------------------------------------------

\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.3cm 0.3cm 0.2cm, clip=true, totalheight=0.3\textheight]{Figures/openmrs_arch.png}
\caption[The OpenMRS architecture]{The OpenMRS Architecture}
\label{OpenMRS architecture}
\end{figure}

%--------------------------------------------------------------------------------------------


The backbone of OpenMRS lies in its core API. The OpenMRS API has methods for all of the basic functions such as adding/updating a
patient, encounter, observation, etc. Methods which enable this functionality are provided in service layer classes.
In the OpenMRS framework and modules, there are different levels in the code architecture. The source code is divided into three
main segments: the User Interface, Service, and Data Access layers. This layering isolates various system responsibilities from one another, to improve both system development and maintenance.


\subsubsection{The Data Access Layer}

The Data Access layer is an abstraction layer from the actual data model and its changes. It uses Hibernate as the Object Relational
mapping tool, and Liquibase to manage relational database changes in a database-independent way. The relationships between our domain objects and database tables are mapped using a mixture of Hibernate annotations and XML mapping files. The data access layer is exposed to the service layer through interfaces, thereby shielding it from from implementation details such as which object relational mapping tool is being used.

\subsubsection{The Service Layer}

The Service layer is responsible for managing the business logic of the application. It is built around the Spring framework. The OpenMRS service layer classes make extensive use of the Spring framework for a number of tasks including the following: 


\begin{itemize}
\item \textit{\texttt{Spring Aspect Oriented Programming (AOP) is used to provide separate cross cutting functions (for example: authentication, logging).}}  
\item \textit{\texttt{Spring Dependency Injection (DI) is used to provide dependencies between components.}}
\item \textit{\texttt{Spring is used to manage transactions in between service layer classes.}}
\end{itemize}


\subsubsection{The User Interface Layer}

The User Interface layer for the legacy application is built upon Spring MVC, Direct Web Remoting (DWR), JSP and JavaScript. DWR is used for AJAX functionality and it provides the mapping between our Java objects and methods to JavaScript objects and methods respectively. 

JQuery is used to simplify the interactions with Javascript and the browser. Spring MVC is used to provide the Model-View-Controller
design pattern. Our domain objects serve as the Model. We have a mixture of controllers that subclass Spring's SimpleFormControllers
and those which use Spring's \emph{@Controller} annotation. For the new reference application user interface, we no longer use Spring MVC, DWR or JSP, but heavily use Groovy, JQuery, AngularJS, and more.

However, at the heart of OpenMRS, there is a custom module framework which enables the extensibility and modification of the default functionality of the OpenMRS core in accordance to your needs. Modules are also structured like the OpenMRS core, and consist of user interface, data access and service layers.

Some OpenMRS functionality is pulled out into modules instead of being written into the core application. This allows users to upgrade the content in those modules without having to wait for the next OpenMRS release. Currently, the only core module used in OpenMRS is the Logic Module.


\subsection{Associated Frameworks and Technological Stack}

\subsubsection{Hibernate}

Hibernate is the \emph{Object Relational Mapper} used by OpenMRS. It allows users to describe the relationship between database tables and domain objects using just xml or annotations. Hibernate is also useful in managing dependencies between classes. As an example, the concept domain in the data model consists of tables named concept, concept\_answer, concept\_set and concept\_name. It would be very difficult to keep up with where to store each part of the concept object and the relations between them if a user decides to update each table individually. 

However, using Hibernate, developers only need to concern themselves with the \emph{Concept} object, and not the tables behind that object. The concept.hbm.xml mapping file does the work of knowing that the Concept object contains a collection of \textbf{ConceptSet} objects, a collection of \textbf{ConceptName} objects, etc. However, also note that Hibernate enforces lazy loading - it will not load all associated objects until they are needed. For this reason, you must either \emph{fetch/save/manipulate} your object in the same session (between one open/closeSession) or you must hydrate all object collections in the object by calling the getters (getConceptAnswers, getConceptNames, getSynonyms, etc).


\subsubsection{Spring MVC}

OpenMRS strongly subscribes to the Model-View-Controller pattern. Most controllers included in the OpenMRS core will be SimpleFormControllers and be placed in the \texttt{org.openmrs.web.controller} package. However, some controllers have been rewritten to use Spring 2.5+ annotations, and we recommend that you use these in the future. The model is set up in the controller's formBackingObject, and processed/saved in the processFormSubmission and onSubmit methods. The jsp views are placed in \texttt{/web/WEB-INF/} view. 

Furthermore, not all files served by the webapp are run through Spring. The \texttt{/web/WEB-INF/web.xml} file maps certain web page
extensions to the SpringController. All \texttt{*.form, *.htm, and *.list} pages are mapped. The \emph{SpringController} then uses the mappings in the openmrs-servlet.xml file to know which pages are mapping to which Controller. There are no \textbf{.jsp} pages that are accessed directly. If a page's url is \texttt{/admin/patients/index.htm}, the jsp will actually reside in \texttt{/web/WEB-INF/view/admin/patients/index.jsp}. This is necessary so that we can do the redirect with the SpringController. Because the file being accessed ends with .htm, the SpringController is invoked by the web server. 

When the SpringController sees the url, it simply replaces .htm with .jsp and looks for the file in \texttt{/web/WEB-INF/view/} according to the \textbf{jspViewResolver} bean in openmrs-servlet.xml. If the page being accessed was patient.form, the mapping in the \textbf{urlMapping} bean would have told spring to use the \textbf{PatientFormController} and the \textbf{patientForm.jsp} file.

\subsubsection{Authentication and Authorization}

OpenMRS has a very granulated permissions system. Every action is associated with a Privilege, which in turn can be grouped into Roles. Examples of such privileges are \lq\lq Add Patient\rq\rq, \lq\lq Update Patient\rq\rq, \lq\lq Delete Patient\rq\rq, \lq\lq Add Concept\rq\rq, \lq\lq Update Concept\rq\rq, and more. A Role can also point to a list of inherited roles. The role inherits all privileges from that inherited role. In this way, hierarchies of roles are possible. A User contains only a collection of Roles, not Privileges. These privileges are enforced in the service layer using AOP annotations. 

\subsubsection{Build management}

OpenMRS uses \textbf{Apache Maven} for build management of the OpenMRS core and modules. All information regarding the module being built, its dependencies on other external modules and components, the build order, directories, and required plug-ins are stored in the modules' \textbf{pom.xml} file. Following release, these build artifacts are uploaded and maintained in a maven repository manager. A maven repository manager is used for this purpose due to a number of advantages that it provides. These advantages include:

\begin{itemize}
\item \textit{\texttt{Faster and more reliable builds}}  
\item \textit{\texttt{Improved collaboration}}
\item \textit{\texttt{Component usage visibility}}
\item \textit{\texttt{Enforcement of component standards}}
\end{itemize}

The Maven Repository used by OpenMRS is SonarType Nexus, which can be accessed at http://mavenrepo.openmrs.org/nexus/.


\subsection{OpenMRS Data Model}

OpenMRS invests a continuous effort into shaping the OpenMRS data model using knowledge and experience gathered from practical
experiences from the Regenstrief Institute, Partners in Health, and all of their developmental partners spread across the world. The core of data model addresses the who, what, when, where, and how of medical encounters. The core data model is divided into ten basic domains.

\textbf{1. Concept:} Concepts are defined and used to support strongly coded data throughout the system\\
\textbf{2. Encounter:} Contains the meta-data regarding health care providers interventions with a patient.\\
\textbf{3. Form:} User interface description for the various components.\\
\textbf{4. Observation:} This is where the actual health care information is stored. There are many Observations per Encounter.\\
\textbf{5. Order:} Sequence actions should occur in.\\
\textbf{6. Patient:} Basic information about patients in this system.\\
\textbf{7. User:} Basic information about the people that use this system.\\
\textbf{8. Person:} Basic information about person in the system.\\
\textbf{9. Business:} Non-medical data used to administrate OpenMRS\\
\textbf{10. Groups/Workflow:} Workflows and Cohort data.\\

However, other domains may also be added to the data model via the use of modules.\\


\subsubsection{Metadata}

\begin{figure}[htbp]
\centering
\includegraphics[trim=0.1cm 0.4cm 0.4cm 0.4cm, clip=true, totalheight=0.4\textheight]{Figures/openmrs_datamodel.png}
\caption[The OpenMRS Data Model]{OpenMRS Data Model}
\label{OpenMRS Data Model}
\end{figure}

%--------------------------------------------------------------------------------------------

Metadata is \emph{\lq data about data \rq}. In OpenMRS, metadata represents system and descriptive data such as data types. Metadata are generally referenced by clinical data, but do not represent any patient- specific data. Significant OpenMRS metadata types include:

\begin{itemize}
\item \textit{\texttt{Drugs}}  
\item \textit{\texttt{EncounterRole}}
\item \textit{\texttt{EncounterType}}
\item \textit{\texttt{Concept}}
\item \textit{\texttt{Form}}
\item \textit{\texttt{Program}}
\item \textit{\texttt{Location}}
\item \textit{\texttt{Role}}
\item \textit{\texttt{User}}
\end{itemize}

The OpenMRS source code(\cite{11}) comes with certain metadata included by default. It is recommended not to edit/remove these. Any request to modify/add metadata should be communicated to the Meta-Data/Terminology lead, who is responsible for the oversight of content required for key functionality of the OpenMRS platform.


\clearpage
