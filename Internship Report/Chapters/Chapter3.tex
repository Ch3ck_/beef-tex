%------------------------------------------------------------------------------------------------
\chapter{IMPROVEMENTS TO THE SYSTEM}
\label{Improvements to the Data Migration System}

%----------------------------------------------------------------------------------------------------
%	The Data Import Tool
%----------------------------------------------------------------------------------------------------

\section{The Data Import Tool}

OpenMRS stores data as tuples on OpenMRS tables. Tuples are based on a global concept and can relate to other Tuples. The Data Import Tool (DIT) supports the movement of medical data between various OpenMRS applications. The eSaude Health community of mozambique(\cite{16}) developed a data migration system(\cite{2}) which enables left to right database migration which follows valid and tested insertion rules of data; where tuples with unique identifiers are moved from the source database to the target database following the OpenMRS data model.

The Data Import Tool motivated by the following main advantages:\\
\texttt{(1)} The migration logic is placed in a versatile xls file, it can be changed without having to re-implement the migration tool.\\
\texttt{(2)} The tool is reusable, it can be used to migrate from different SOURCE databases into OpenMRS.\\
\texttt{(3)} The tool is generic, it can be used in different migration contexts that evolve OpenMRS as the TARGET database.\\
\texttt{(4)} The tool is configurable with a user friendly migration settings page. \\
\texttt{(5)} The tool is auditable, hence provides feedback to the user from the UI. \\



%------------------------------------------------------------------------------------------------------
%	The Data Import Tool Architecture
%------------------------------------------------------------------------------------------------------

\subsection{The Data Import Tool Architecture}

Following the Figure 2.4 below, the Data Import Tool is a tool that takes a \emph{matching file} as input, validates the matches, translates them into \emph{SQL SELECT, INSERT and/or UPDATE} statements (mapping), and execute the resulting mapping.

The matches are structured in an \textbf{XLS} input file that contains all the necessary information that the migration tool needs in order to make mapping decisions and generate the actual mapping, e.g.\\

\begin{itemize}
\item \textit{\texttt{Where to get/set value?}}  
\item \textit{\texttt{Where to get the FK’s or referenced values?}}
\item \textit{\texttt{Does the value need to be converted, truncated, etc?}}
\item \textit{\texttt{What is the corresponding value of a particular parameterized value?}}
\end{itemize}

%--------------------------------------------------------------------------------------------

\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.4cm 0.4cm 0.4cm, clip=true, totalheight=0.4\textheight]{Figures/data_import_tool.png}
\caption[Data Import Tool Architecture]{Data Import Tool Architecture}
\label{DIT Architecture}
\end{figure}

%---------------------------------------------------------------------------------------------


The bootstrap configurations are passed as input to the DIT module. It contains the info about the source and target database(s) (drive name, db name, host, port, and password).The DIT module  is a custom developed software that performs in three phases, namely: \emph{validation, translation and execution}. The tool mainly validates all the matches at once, generate the mapping and execute the mapping as it is generated, all is done using automatic processes.

The \textbf{validation manager}(\cite{5}) tells whether or not the matching is correct, prior to the translation process(\cite{6}), based on the info provided by the matching file, e.g.


\begin{itemize}
\item \textit{\texttt{Does the TARGET column datatype matches with the SOURCE column datatype?}}
\item \textit{\texttt{Does the TARGET column size matches with the SOURCE column size?}}
\item \textit{\texttt{Can the TARGET column receive a NULL value, in case the SOURCE column value is NULL?}}
\end{itemize}


%--------------------------------------------------------------------------------------------

\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.3cm 0.3cm 0.3cm, clip=true, totalheight=0.3\textheight]{Figures/validation_mgr.png}
\caption[Validation Manager]{Validation Manager}
\label{Validation Manager}
\end{figure}

%---------------------------------------------------------------------------------------------


The validation is reported in form of info, error or warning that can be either used by the user in order to adjust or fix the matching, or by the tool in order to make matching decisions. In case of severity status in the report, the tool prevents the flow from going into the translation phase until the severity is fixed by the user. During the validation phase, the tool encapsulates the matches in Java POJO objects structured based on composite design pattern. The composed objects are further used by the translation process.

%--------------------------------------------------------------------------------------------

\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.3cm 0.3cm 0.3cm, clip=true, totalheight=0.3\textheight]{Figures/translation_mgr.png}
\caption[Translation Manager]{Translation Manager}
\label{Translation Manager}
\end{figure}

%---------------------------------------------------------------------------------------------


The DIT module creates the mapping and execute the mapping queries on-the-fly into the TARGET database and optionally generates an SQL output file for further use. During the validation, translation and execution phases the translation tool reports all actions in a log file and the User Interface (UI) that can be used for auditing purposes.


\subsubsection{The Matching file}

This is the \emph{key} file(\cite{3}) in the Data migration process. It contains all the information on how the tuples are mapped between both databases. It consists of the following:\\

\textbf{Tuple Sheet}\\
This deals with what data to migrate to the OpenMRS table. It contains information about the OpenMRS table where it goes to and a unique identifier together with the concept name it's based on. This sheet is subsequently used by other sheets in the translation process.\\

\textbf{Match L to R Sheet}\\
This sheet contains the information about the two databases involved in the migration with left corresponding to the OpenMRS(target) database and the right being the source database. It contains the mappings between target and source used by the DMT engine to process.\\

\textbf{L Reference Sheet}\\
It contains relationship references of the target database, foreign key columns of tuples and their respective values. This sheet can be of two types; either directly referencing the tuple tables with a foreign key or indirectly.\\

\textbf{R Reference Sheet}\\
This sheet is used by the translation engine of the DMT and contains information on how to retrieve data from the left side database. It indicates how to get the \textbf{CURRS}\footnote{\emph{A CURR} is the value(s) of the parameter(s) used to select a parent tuple.} of a tuple and conditions to retrieve data for matching. This is applied during data export/retrieval from the OpenMRS tables.\\

\textbf{Value Match Sheet}\\
This sheet represents the correspondences between the values of the equivalent groups on both sides of the migration. So this basically takes values from the right, finds it's equivalent and sets it to the left side of the match. These tables are used by the translation engine in moving data from the source to target OpenMRS table.\\

\clearpage % Starts a new page.

\subsection{The OpenMRS module architecture}

Below is the code breakdown of the OpenMRS module architecture(\cite{14}).

\begin{itemize}
\item \textbf{api - \textit{non-web-specific 'maven module' project}}
	\begin{itemize}
	\item \textbf{src}
		\begin{itemize}
		\item \textit{main - Java files in the module that are not web-specific. These will be compiled into a distributable mymodule.jar}
		\end{itemize}
	\item \textit{target - folder built at runtime that will contain the distributable jar file for the module omod}
	\end{itemize}
\item \textbf{omod}
	\begin{itemize}
	\item \textbf{src}
		\begin{itemize}
		\item \textbf{main}
			\begin{itemize}
				\item \textit{java - web specific java files like controllers, servlets, and filters}
				\item \textbf{resources}
					\begin{itemize}
					\item \textit{config.xml}
					\item \textit{*.hbm.xml files}
					\item \textit{liquibase.xml (or the old sqldiff.xml )}
					\item \textit{messages_*.properties files}
					\item \textit{modulesApplicationContext.xml}
					\item \textit{log4j.xml - optional file to control logging in your module}
					\end{itemize}
				\item \textbf{webapp - jsp and html files included in the omod}
				\begin{itemize}
					\item \textit{portlets}
					\item \textit{resources - image, js, and css files that your jsp files reference}
				\end{itemize}
			\end{itemize}
		\end{itemize}
	\item \textbf{target - \textit{target - Contains the distributable omod file}}
	\end{itemize}
\item \textbf{pom.xml - \textit{Maven build file. Delegates to pom.xml files in the omod and api project}}
\end{itemize}


\clearpage
