% Chapter 1
\chapter{INTRODUCTION} % Main chapter title
\label{Introduction} % For referencing the chapter elsewhere, use \ref{Chapter1} 
%\lhead{Chapter 1. \emph{Introduction}} % The header on each page

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}


%----------------------------------------------------------------------------------------
% SECTION ON HISTORY OF OPENMRS
%----------------------------------------------------------------------------------------
\section{Historical Background}

For many years, Health IT applications have been created and used to effectively record and manage patient medical records. Traditionally, patient data were recorded in paper records. However, advances in the field of medicine are introducing the need to manage very large amounts of data. Because paper medical records are inherently passive, they can not evaluate or trigger meaningful actions in response to their content. These challenges led to the development of many different electronic medical record (EMR) systems. These software tools promote meaningful use of patient health records. Over the past several decades, several commercial and open source EMR tools have been developed and implemented with varied levels of success. 

\subsection{Introduction to OpenMRS}

\textbf{OpenMRS}(\cite{1}; \url{http://openmrs.org}) was conceived in 2004 specifically to solve the problem of managing health care information in the developing world. Today, connectivity and accessibility are critical pieces for health information systems. 

\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.2cm 0.2cm 0.1cm, clip=true, totalheight=0.2\textheight]{Figures/openmrs_logo.jpg}
\caption[The OpenMRS Logo]{OpenMRS Logo}
\label{omrs_logo}
\end{figure}

In most countries, this information is still in silos and is not accessible to those who need it---patients, clinicians, researchers, epidemiologists, and planners. Based on best practices and institutional knowledge from founding partners \texbtf{Regenstrief Institute & Partners In Health}, the goal of OpenMRS was to become a platform that could be flexible enough for use in a variety of contexts in settings that had very different requirements. Both organizations knew they were doing similar work and wanted to work together to build a common platform to save time and effort. Late in 2004, \emph{Ben Wolfe} from \emph{Regenstrief Institute} became the first full-time programmer working on OpenMRS, and \emph{Darius Jazayeri} from \emph{Partners In Health} soon followed. 

For ease of work and other practical reasons, a project wiki was set up and an online instance of subversion (\cite{7}, \url{http://sourceforge.net/openmrs}) was used for source control. Over time, word spread about the project and to cater for increasing development needs, it was moved to Github(\cite{11}). The group didn't set out to create an open source software project initially, but it quickly became evident that is what was needed. OpenMRS first "went live" in February 2006 at the AMPATH project in Western Kenya. Partners In Health turned on OpenMRS in Rwinkwavu, Rwanda, in August of the same year. The South African Medical Research Council first launched on the system at Richmond Hospital in KwaZulu-Natal at the end of 2006.


%--------------------------------------------------------------------
\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.3cm 0.3cm 0.2cm, clip=true, totalheight=0.3\textheight]{Figures/ampath.png}
\caption[AMPATH project]{AMPATH project started at Moi Hospital Nairobi, Kenya}
\label{Ampath Project}
\end{figure}

%-------------------------------------------------------------------

Since then, the rate of installation and use of OpenMRS has continuosly increased at a rapid pace. The software has been downloaded in nearly every country on the planet and is used in implementations from single traveling clinics to nation-wide installations in hospitals and clinics throughout countries like Rwanda.

However, OpenMRS is a Java-based web application capable of running on laptops in small clinics or large servers for nation-wide
use. This platform improves health outcomes by providing a timely, comprehensive, and coordinated foundation for delivery of health care. Add-on modules created by other users allow functionality to be easily added or removed from the system. This modular architecture allows users to customize OpenMRS to local health care needs, and reduces the need for custom programming.
OpenMRS has served as a training platform for developers since its beginning. 

This project has participated in \emph{Google Summer of Code} (GSoC, \url{http://summerofcode.withgoogle.com}) since 2007, offering university students a chance to practice software development as well as free and open source project management skills. Many training programs have flourished throughout the developing world, increasing the number of people with technological and entrepreneurial skills to support Health IT implementations. The community has assisted in facilitating training programs in places like Rwanda that teach students to develop medical information systems like OpenMRS.


\subsection{The OpenMRS community}

Volunteers from around the world have created the OpenMRS Community, a group of talented individuals from many different backgrounds including technology, health care, and international development. Together, they are building the world's most comprehensive and flexible health technology platform to support the delivery of health care in some of the world's most challenging environments.

The OpenMRS organization specifically responds to the needs of people actively building and managing health systems in the developing world; places where AIDS, tuberculosis, and malaria afflict the lives of millions. It initially started out the help a single clinic in Kenya, but in the last few years OpenMRS has grown dramatically to be used in thousands of research and clinical settings across the planet. Recent releases of the OpenMRS core application consistently had between 50 and 100 contributors.

%--------------------------------------------------------------------
\begin{figure}[htbp]
\centering
\includegraphics[trim=0.2cm 0.3cm 0.3cm 0.2cm, clip=true, totalheight=0.3\textheight]{Figures/openmrs_atlas.png}
\caption[OpenMRS Atlas]{OpenMRS adoption worldwide}
\label{OpenMRS Atlas}
\end{figure}

%-------------------------------------------------------------------

The purpose of this organization(\cite{13}) is to provide technical infrastructure and community management, to assist collaboration and cooperation of project volunteers throughout the world, and to provide training and support to those who seek to implement OpenMRS as a key part of a medical informatics strategy in clinics, hospitals, and government health organizations.
The mission of the OpenMRS community is \textit{to improve health care delivery in resource-constrained environments by working together as a global community to create a robust, scalable, user-driven, open source medical record system platform.}
Between 2006 and 2012, the installation of OpenMRS at AMPATH in Kenya has recorded over 5 million heath care encounters points of data for nearly 200,000 patients, helping to save untold thousands of lives. 



%----------------------------------------------------------------------------------------
% SECTION ON DATA MIGRATION TO OPENMRS
%----------------------------------------------------------------------------------------

\section{Data Migration to OpenMRS}

OpenMRS stores it's data as \emph{tuples} on OpenMRS tables. Tuples are based on a global concept and can relate to other Tuples. Data migration is an essential aspect of OpenMRS Implementer environment which increases collaboration between different OpenMRS implementers or easy movement of electronic medical record information from one database to another. Support for import and export of data in various OpenMRS applications is a high priority project for OpenMRS. 

eSaude data migration tool (\cite{9}) is a data migration tool which enables the left(source) to right(target) migration of data from source to OpenMRS database and vice versa. It follows valid and tested insertion rules of data where Tuples with unique identifiers are easily imported to corresponding OpenMRS tables respecting the OpenMRS table settings. This facilitates the upgrade of medical record systems from other EMRs to OpenMRS. The Data Import Tool module aims at creating an easy data import process to OpenMRS.


%----------------------------------------------------------------------------------------
% REPORT ORGANIZATION
%----------------------------------------------------------------------------------------

\section{Report Organization}

This document consists of six (6) chapters. Chapter 2 reviews literature on the data migration tool for OpenMRS. Chapter 3  states the improvements to be made to the existing system. Chapter 4 discusses the implementation methodology used together with justifications. Chapter 5 presents the results obtained and sample use cases for the Data Import Tool module. Finally, Chapter 6 states the conclusions drawn from the work and further directions in which the module can be improved.

\begin{flushright}
\end{flushright}
