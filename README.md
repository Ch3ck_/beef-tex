# Beef-Tex
LaTex Markups for GSoC Internship projects

# Compile instructions.

cd /proper_dir

````
  $ biber main
  $ pdflatex main.tex
  $ R
  
````
This generates the pdf files.
